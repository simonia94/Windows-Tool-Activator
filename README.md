<h1 align="center">
  <br>
  <a href="https://officialsimonia94.wordpress.com/"><img src="https://github.com/simonia94/simonia94/blob/main/20210804115011.jpg"></a>
  <br>
  Windows Tool Activator
  <br>
</h1>

<p align="center">
<a href="https://discord.io/simonia94">
    <img src="https://discordapp.com/api/guilds/853681828501127178/widget.png?style=shield" alt="Discord Server">
  </a>
  <a href="https://officialsimonia94.wordpress.com/">
    <img src="https://img.shields.io/badge/Site-Simonia94-blue?style=flat-square&logo=appveyor">
  </a>
  </p>
  
# Description du script
Le script a été écrit en langue Batch pour activer Windows 10/11 gratuitement.<br>
Attention, le script doit être exécuter en administrateur sinon l'activation risque d'échoué.

# Fonctions
- Activer Windows
- Activation à l'aide une clé Windows
- Désinstallation de la licence Windows
- Vérifier l'état d'activation Windows

